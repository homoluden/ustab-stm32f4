#ifndef __SPI_H
#define __SPI_H

#include <stm32f4xx.h>
#include <stm32f4xx_rcc.h>
#include <stm32f4xx_gpio.h>
#include <stm32f4xx_tim.h>
#include <stm32f4xx_spi.h>
#include <misc.h>

void init_SPI1(void);

/* This funtion is used to transmit and receive data
 * with SPI1
 *          data --> data to be transmitted
 *          returns received value
 */
uint8_t SPI1_send(uint8_t data);

#endif // __SPI_H

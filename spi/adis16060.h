#ifndef __ADIS16060_H
#define __ADIS16060_H

#include <stm32f4xx.h>
#include <stm32f4xx_rcc.h>
#include <stm32f4xx_gpio.h>
#include <stm32f4xx_tim.h>

#include "main.h"
#include "spi.h"

#define SPI_SCK_SPARE GPIOA, GPIO_Pin_4
#define G1_MSEL1 GPIOC, GPIO_Pin_4
#define G1_MSEL2 GPIOC, GPIO_Pin_5
#define G2_MSEL1 GPIOC, GPIO_Pin_2

#define PARSE_ADIS(buf)  ((((buf[0] & 0b00000011) << 4) + (buf[1] >> 4)) << 8) + ((buf[1] & 0b00001111) << 4) + (buf[2] >> 4)

void init_int_tim2(void);

void init_gyros(void);

void adis_stream_EN(void);
void adis_stream_DIS(void);

void SCK_manual_switch(u8 times);

void G_config(u8 value);

void G1_ReadWrite(u8* buf);
void G2_ReadWrite(u8* buf);

#endif

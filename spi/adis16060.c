#include "adis16060.h"

void init_int_tim2(void)
{
    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
    NVIC_InitTypeDef NVIC_InitStructure;
    /* Enable the TIM2 global Interrupt */
    NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    /* TIM2 clock enable */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
    /* Time base configuration */
    TIM_TimeBaseStructure.TIM_Period = 1000 - 1; // 1 MHz down to 1 KHz (1 ms)
    TIM_TimeBaseStructure.TIM_Prescaler = 84 - 1; // 24 MHz Clock down to 1 MHz (adjust per your clock)
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);
    /* TIM IT enable */
    TIM_ClearITPendingBit ( TIM2, TIM_IT_Update );
    TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
    /* TIM2 disable counter until streaming is On by external command*/
    TIM_Cmd(TIM2, DISABLE);
}

void init_gyros(void)
{
    init_SPI1();

    GPIO_InitTypeDef GPIO_InitStruct;

    // enable clock for used IO pins
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA | RCC_AHB1Periph_GPIOC, ENABLE);

    GPIO_InitStruct.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_4 | GPIO_Pin_5;
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;

    GPIO_Init(GPIOC, &GPIO_InitStruct);

    //GPIO_ResetBits(SPI_SCK_SPARE); // set SCK_SPARE high

    GPIOC->BSRRL |= GPIO_Pin_2 | GPIO_Pin_5 | GPIO_Pin_4; // set G1_MSEL2 and G1_MSEL1 high
}

void G_config(u8 value)
{
    GPIO_ResetBits(G1_MSEL2);
    SPI1_send(value);
    GPIO_SetBits(G1_MSEL2);
}

void G1_ReadWrite(u8* buf)
{
    GPIO_ResetBits(G1_MSEL1);

    //Delay(1);

    buf[0] = SPI1_send(buf[0]);
    buf[1] = SPI1_send(buf[1]);
    buf[2] = SPI1_send(buf[2]);

    GPIO_SetBits(G1_MSEL1);
}


void G2_ReadWrite(u8* buf)
{
    GPIO_ResetBits(G2_MSEL1);

    //buf[0] = buf[1] = buf[2] = 0;

    buf[0] = SPI1_send(buf[0]);
    buf[1] = SPI1_send(buf[1]);
    buf[2] = SPI1_send(buf[2]);

    GPIO_SetBits(G2_MSEL1);
}

void TIM2_IRQHandler(void)
{
    if (TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET)
    {
        TIM_ClearITPendingBit(TIM2, TIM_IT_Update);


    }
}

void adis_stream_EN(void)
{
    TIM_Cmd(TIM2, ENABLE);
}
void adis_stream_DIS(void)
{
    TIM_Cmd(TIM2, DISABLE);
}

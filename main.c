#include "main.h"

#include "leds_control.h"
#include "spi/adis16060.h"

/* Globals ---------------------------------------------*/
char streaming_on = 0;

int i = 0, j = 0, k = 0, l = 0;
char USART_Buffer[RX_BUF_SIZE];
char initialized = 0;

unsigned int time_stamp = 0;
/*------------------------------------------------------*/

int main(void)
{
    SystemInit();

    Gpio_Init();

    UB_Uart_Init();

    init_gyros();

    while(1) {

        // TODO: State Processing code goes here

        Handle_RemoteCommand();
    }
}

void Handle_RemoteCommand()
{
    UART_RXSTATUS_t rcv_status;
    u16 cmd;
    char cmd_buf[RX_BUF_SIZE];

    rcv_status = UB_Uart_ReceiveString(COM2, cmd_buf);

    if (rcv_status == RX_EMPTY) {
        return;
    }
    else if(rcv_status == RX_FULL){
        cmd = CMD_ERR_TRUNC;
    }
    else {
        cmd = (cmd_buf[0] << 8) + cmd_buf[1];
    }

    Parse_RemoteCommand(cmd, cmd_buf);
}

void Parse_RemoteCommand(u16 cmd, char *buf)
{
    if (cmd == CMD_STREAM_STATE) {
        if (buf[2] == CMD_OPT_ON) {
            streaming_on = 1;
        }
        else if (buf[2] == CMD_OPT_OFF) {
            streaming_on = 0;
        }
    }
    else if (cmd == CMD_LED_R) {
        switch_led_red(buf[2]);
    }
    else if (cmd == CMD_LED_G) {
        switch_led_green(buf[2]);
    }
    else if (cmd == CMD_LED_B) {
        switch_led_blue(buf[2]);
    }
    else if (cmd == CMD_PWM_RGB) {
        char *pwm_str = &buf[2];
        char * ptr_end;
        long int red, green, blue;

        red = strtol (pwm_str,&ptr_end,10);
        green = strtol (ptr_end,&ptr_end,10);
        blue = strtol (ptr_end,NULL,10);

        LEDRed_PWM_set((u32)red);
        LEDGreen_PWM_set((u32)green);
        LEDBlue_PWM_set((u32)blue);
    }
    else if (cmd == CMD_READ_GYROS) {
        unsigned int g1, g2, g3, g4 = 0;
        u8 buf[3] = {0,0,0};

        G_config(0b00100000);

        G1_ReadWrite(buf);
        g1 = PARSE_ADIS(buf);

        // FIXME: find out why sometimes first read returns 0b11111111111111
//        if (g1 == 16383) {
//            G1_ReadWrite(buf);
//            g1 = PARSE_ADIS(buf);
//        }

        G2_ReadWrite(buf);
        g2 = PARSE_ADIS(buf);

        G1_ReadWrite(buf);
        g3 = PARSE_ADIS(buf);

        G2_ReadWrite(buf);
        g4 = PARSE_ADIS(buf);

        char data[64] = {0, };
        sprintf(data, "t:%u;g:%u;%u;%u;%u", time_stamp++, g1,g2,g3,g4);
        UB_Uart_SendString(COM2, data , CRLF);
    }
}

void Gpio_Init(void)
{

//    GPIO_InitTypeDef  GPIO_InitStructure;

    /* GPIOD Periph clock enable */
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
    //RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
    //RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE) ;     // TIM4 Periph clock enable

    GPIO_PinAFConfig( GPIOB, GPIO_PinSource7, GPIO_AF_TIM4 );
    GPIO_PinAFConfig( GPIOB, GPIO_PinSource8, GPIO_AF_TIM4 );
    GPIO_PinAFConfig( GPIOB, GPIO_PinSource9, GPIO_AF_TIM4 );

    Initialize_Leds_GPIO(LEDRGB_Out);

    // Initialize TIM4
    /*
        Set timer period when it have reset
        First you have to know max value for timer
        In our case it is 16bit = 65535
        To get your frequency for PWM, equation is simple

        PWM_frequency = timer_tick_frequency / (TIM_Period + 1)

        If you know your PWM frequency you want to have timer period set correct

        TIM_Period = timer_tick_frequency / PWM_frequency - 1

        In our case, for 10Khz PWM_frequency, set Period to

        TIM_Period = 84000000 / 10000 - 1 = 8399

        If you get TIM_Period larger than max timer value (in our case 65535),
        you have to choose larger prescaler and slow down timer tick frequency
    */
    Initialize_Leds_Timer ( TIM4, LED_PWM_Prescaler, LED_PWM_Period-1 );

    // Initialize PWMs 2-4
    Initialize_Leds_PWM ( TIM4 );
}

void Initialize_USR()
{
    UB_Uart_SendByte(COM2, '+');
    Delay(240000L);
    UB_Uart_SendByte(COM2, '+');
    Delay(240000L);
    UB_Uart_SendByte(COM2, '+');
    Delay(240000L);

    UB_Uart_ReceiveString(COM2, USART_Buffer);
    Delay(48000L);

    UB_Uart_SendByte(COM2, 'a');
    Delay(48000L);

    UB_Uart_ReceiveString(COM2, USART_Buffer);

    Delay(240000L);

    UB_Uart_SendString(COM2, "AT+H", CRLF);

    UB_Uart_ReceiveString(COM2, USART_Buffer);
    Delay(240000L);

    UB_Uart_SendString(COM2, "AT+UART=460800,8,1,None,NFC", CRLF);
}

void Delay(__IO uint32_t nCount)
{
  while(nCount--)
  {
  }
}

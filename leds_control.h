#ifndef __LEDS_CTRL_H
#define __LEDS_CTRL_H

//   By default in stm32f4discovery project the expected 25MHz HSE crystal.
// But in discovery board HSE = 8MHz. Check value and change as follows
// PLL_M     to                  8  in "system_stm32f4xx.c" file
// HSE_VALUE to ((uint32_t)8000000) in "stm32f4xx.h"        file
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stm32f4xx_gpio.h>
#include <stm32f4xx_rcc.h>
#include <stm32f4xx_tim.h>
#include "stm32f4xx.h"

// DEFINES

#define LEDRed_Out GPIOB, GPIO_Pin_7
#define LEDGreen_Out GPIOB, GPIO_Pin_8
#define LEDBlue_Out GPIOB, GPIO_Pin_9
#define LEDRGB_Out GPIOB, GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9

#define LEDRed_Active 1
#define LEDGreen_Active 2
#define LEDBlue_Active 3

#define LED_PWM_Prescaler 0 // Divider of Timer Clock Freq. == (LED_PWM_Prescaler + 1)
#define LED_PWM_Period 8400 // Timer Clock / (LED_PWM_Period + 1) => PWM Freq. (84MHz / 8k4 = 10kHz)

/// defines

// PROTOTYPES
void switch_led_red(REMOTE_CMD_OPT_t opt);
void switch_led_green(REMOTE_CMD_OPT_t opt);
void switch_led_blue(REMOTE_CMD_OPT_t opt);

void LEDRed_PWM_set(u32 pwm);
void LEDGreen_PWM_set(u32 pwm);
void LEDBlue_PWM_set(u32 pwm);

void Initialize_Leds_GPIO ( GPIO_TypeDef* GPIOx, u16 Pin ) ;
void Initialize_Leds_Timer ( TIM_TypeDef* TIMx, u16 PrescaleDiv, u16 Period ) ;
void Initialize_Leds_PWM ( TIM_TypeDef* TIMx ) ;

/// prototypes

// GLOBALS
char active_led;
/// globals

#endif // __LEDS_CTRL_H

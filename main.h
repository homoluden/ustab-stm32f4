#ifndef __MAIN_H
#define __MAIN_H

#include <string.h>
#include "stm32f4xx.h"
#include <stdio.h>
#include <strings.h>
#include "stm32f4xx_usart.h"
#include "stm32_ub_uart.h"
#include <stm32f4xx_gpio.h>
#include <stm32f4xx_rcc.h>


/* Prototypes ------------------------------------------*/
void Delay(__IO uint32_t nCount);
void Gpio_Init(void);
void Usart_Init(void);
void USART_Putc(volatile char c);
void USART_Puts(char* str);
void USART2_IRQHandler(void);
void USART_InsertToBuffer(u8 c);
u8 USART_Getc(void);
u16 USART_Gets(u8* buffer, u16 bufsize);
u8 USART_BufferEmpty(void);

void Initialize_USR();

void Handle_RemoteCommand();
void Parse_RemoteCommand(u16 cmd, char *buf);
/*------------------------------------------------------*/


/* Defines ---------------------------------------------*/

typedef enum {
    // State Commands (numbers should be in range of [RX_FIRST_CHR... RX_LAST_CHR])
    CMD_STREAM_STATE = (u16)((33<<8) + 33), // State streaming On / Off

    CMD_LED_R = (u16)((34<<8) + 33), // LED1 On / Off
    CMD_LED_G = (u16)((34<<8) + 34), // LED2 On / Off
    CMD_LED_B = (u16)((34<<8) + 35), // LED3 On / Off
    CMD_PWM_RGB = (u16)((34<<8) + 39), // RGB LED PWMs set. Example for CMD value: "8400 4200 2100"
    CMD_READ_GYROS = (u16)((34<<8) + 40), // Readout all Gyros

    // Errors
    CMD_ERR_TRUNC = (u16)((0x7E<<8) + 0x7E), // Buffer was full. Command content probably truncated.

}REMOTE_CMD_t;
typedef enum {
    CMD_OPT_ON = 113,   // Last simple num in RX_..._CHR range
    CMD_OPT_OFF = 37    // First simple num in RX_..._CHR range
}REMOTE_CMD_OPT_t;
/*------------------------------------------------------*/

#endif // __MAIN_H
